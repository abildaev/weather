import React from 'react';
import classes from './Info.module.css';
import {MdHighlightOff} from "react-icons/md";
import WeatherIcon from 'react-icons-weather';



const InfoOnline = (props) => {


    return (
        <div>
            {props.city &&
            <div className={classes.info}>
                <div className={classes.info_text}>{props.city}-{props.country}</div>
                <div className={classes.info_text}>{props.temp + '\xB0'}</div>

                <WeatherIcon className={classes.weatherIcon} name="owm" iconId={props.id} flip="horizontal" rotate="90" />
                <div className={classes.info_line}></div>

                <div>
                    <div>Sunset</div>

                    <div className={classes.info_group}>
                        <div>{props.sunset}</div>
                    </div>

                </div>
            </div>}

            {props.error &&
            <div className={classes.error}>
                <MdHighlightOff className={classes.icon}/>
                <div>{props.error}</div>
            </div>

            }


        </div>

    )
}

export default InfoOnline;