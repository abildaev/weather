import React, {useState} from 'react';
import classes from './NavBar.module.css';
import {NavLink} from "react-router-dom";
import {MdDehaze, MdClose} from "react-icons/md";


const NavBar = () => {

    const [active, setActive] = useState(false);
    const closeMenu = <MdClose className={classes.hamburger_trigger}/>;
    const menu = <MdDehaze className={classes.hamburger_trigger}/>;
    return (
        <div>
            <div  onClick={() => setActive(!active)}>{active ? closeMenu : menu}</div>
            <ul className={classes.nav} style={{left: active ? '0':''}}>
                <li><NavLink to='/info' className={classes.link}>онлайн</NavLink></li>
                <li><NavLink to='/week' className={classes.link}>на 7 дней</NavLink></li>
            </ul>
        </div>


    )
}

export default NavBar;

