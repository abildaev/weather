import React from 'react';
import {MdHighlightOff} from "react-icons/md";
import classes from './WeatherWeek.module.css';


const WeatherWeek = (props) => {


    return (
        <div className={classes.component}>
            {props.city &&
                <div>
                    <div>{props.city}-{props.country}</div>
                    <div className={classes.component}>{props.info}</div>
                </div>

                }



                {props.error &&
                <div className={classes.error}>
                <MdHighlightOff className={classes.icon}/>
                <div>{props.error}</div>
                </div>

                }
        </div>





)


}


export default WeatherWeek;