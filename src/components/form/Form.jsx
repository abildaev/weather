import React from 'react';
import classes from './Form.module.css'
import {MdSearch} from "react-icons/md";


const Form = (props) => {


    return (
        <form className={classes.form} onSubmit={props.weatherMethod} >
            <input className={classes.form__search} type="text" name="city" placeholder="Введите город"/>
            <button className={classes.form__btn} ><MdSearch/></button>
        </form>
    );

}
export default Form;