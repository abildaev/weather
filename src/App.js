import React, {useState} from 'react';
import './App.css';
import {BrowserRouter, Route} from "react-router-dom";
import Form from './components/form/Form';
import InfoOnline from './components/weather_info/InfoOnline';
import WeatherWeek from './components/weatherWeek/WeatherWeek';
import NavBar from './components/navbar/NavBar';
import WeatherIcon from 'react-icons-weather';
import moment from 'moment';
import 'moment/locale/ru';
moment.locale('ru');



const ApiKey = 'fd1f53f8177eb0c4a8d64745c939c06a';
const ApiKey2 = '4ea584bbaaa643258bd7de37b410b274';


const App = () => {
    const [weather, setWeather] = useState(0);
    const [weekInfo, setweekInfo] = useState(0);

    const gettingWeather = async (e) => {
        e.preventDefault();
        const city = e.target.elements.city.value;


        if (city) {
            const api_url = await fetch
            (`http://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${ApiKey}&units=metric`);
            const data = await api_url.json();
            console.log(data);

            if (data.cod === 200) {
                const sunset = data.sys.sunset;
                const date = new Date();
                date.setTime(sunset);
                const date_sunset = date.getHours() + ":" + date.getMinutes();

                let weatherId = data.weather[0].id;

                setWeather({
                    cod: 200,
                    city: data.name,
                    temp: data.main.temp_max,
                    country: data.sys.country,
                    sunset: date_sunset,
                    id: weatherId,
                    error: undefined,
                })

                const api_urlWeek = await fetch
                (`https://api.weatherbit.io/v2.0/forecast/daily?city=${city}&key=${ApiKey2}`);
                const dataWeek = await api_urlWeek.json();
                console.log(dataWeek);

                setweekInfo ({
                    info: dataWeek.data.slice(0, 7).map(e => (
                        <div>
                            <div style={{display: 'flex', justifyContent: 'space-around', alignItems: 'center'}}>
                                <div>{moment(e.valid_date).format('dd DD MMM')}</div>
                                <WeatherIcon  name="owm" iconId={e.weather.code} flip="horizontal" rotate="90" />
                                <div>{Math.round(e.app_max_temp) + '\xB0C'}</div>
                                <div>{Math.round((e.app_max_temp) * 1.8 + 32) + '\xB0F'}</div>
                                <div>{Math.round((e.app_max_temp) + 273.15) + '\xB0K'}</div>
                            </div>
                            <div style={{width: '100%', height: '1px', background: 'rgba(44, 62, 80,.2)', margin: '10px 0'}}></div>
                        </div>
                    ))
                })
            } else {
                setWeather({
                    error: "Город не найден!"
                })
            }
        } else {
            setWeather({
                error: "Введите название города!"
            })
        }
    }

    return (
        <BrowserRouter>
            <div className='wrapper'>
                <div className='container'>
                    <NavBar/>
                    <Form weatherMethod={gettingWeather}/>
                    <Route path='/(info| |)' render={() => <InfoOnline city={weather.city}
                                                                      temp={weather.temp}
                                                                      country={weather.country}
                                                                      sunset={weather.sunset}
                                                                      id={weather.id}
                                                                      error={weather.error}/>}/>

                    <Route path='/week' render={() => <WeatherWeek city={weather.city}
                                                                   country={weather.country}
                                                                   info ={weekInfo.info}
                                                                   error={weather.error}/>}/>
                </div>
            </div>
        </BrowserRouter>
    );

}

export default App;



